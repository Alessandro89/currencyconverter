from urllib.request import urlopen
import unittest
import endpointConverter
import json
from decimal import *

class TestConvertApi(unittest.TestCase):
    def setUp(self):
        urlExchangeCurrency = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml"
        self.management = endpointConverter.ManagementCurrency(urlopen(urlExchangeCurrency))
        self.app = endpointConverter.app.test_client()
        self.dictCurrencyDate = self.management.dictCurrencyDate

    def getUrlConvert(self,amount,srcCurrency,destCurrency,referenceDate):
        return "http://localhost:5000/convert?amount={}&src_currency={}&dest_currency={}&reference_date={}".format(amount,srcCurrency,destCurrency,referenceDate) 

    def testGetConvertMissParameters(self):
        response = self.app.get('http://localhost:5000/convert')
        self.assertEqual(response.status_code, 400)
        response = self.app.get('http://localhost:5000/convert?amount=400')
        self.assertEqual(response.status_code, 400)
        response = self.app.get('http://localhost:5000/convert?amount=400&src_currency=Eur&dest_currency=chf')
        self.assertEqual(response.status_code, 400)

    def testGetConvertWrongAmount(self):
        response = self.app.get(self.getUrlConvert("-400","Eur","chf","2018-07-1"))
        self.assertEqual(response.status_code, 400)

    def testGetConvertWrongSrcAmount(self):
        response = self.app.get(self.getUrlConvert("400","asdf","chf","2018-07-1"))
        self.assertEqual(response.status_code, 400)
        
    def testGetConvertWrongDestAmount(self):
        response = self.app.get(self.getUrlConvert("400","chf","asdf","2018-07-1"))
        self.assertEqual(response.status_code, 400)
    
    def testGetConvertWrongFormatDate(self):
        response = self.app.get(self.getUrlConvert("400","chf","asdf","10-07-2018"))
        self.assertEqual(response.status_code, 400)


    def testGetConvertWrongOldDate(self):
        response = self.app.get(self.getUrlConvert("400","chf","asdf","2017-07-10"))
        self.assertEqual(response.status_code, 400)

    def testGetConvertOk(self):
        adate = list(self.dictCurrencyDate)[0]
        amount = 400
        scrCurrency = "USD"
        destCurrency = "GBP"
        response = self.app.get(self.getUrlConvert(amount,scrCurrency,destCurrency,adate))
        amountDest = self.management.calculateExchangeCurrency(self.dictCurrencyDate[adate],amount,scrCurrency,destCurrency)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.get_data().decode()), {'amount': str(amountDest), 'currency': destCurrency})

#test ManagementCurrency:
lenListCurrencyDate = 63
lenListCurrency = 33
class TestManagementCurrency(unittest.TestCase):
    def setUp(self):
         #cambiamo file per il test..avendo così dei dati fissi
        self.management = endpointConverter.ManagementCurrency(open("fileConverterTest.xml","r"))
        self.listCurrency  = self.management.listCurrency
        self.dictCurrencyDate = self.management.dictCurrencyDate
        
    def testReadCurrencyFile(self):
        self.assertEqual(len(self.dictCurrencyDate),lenListCurrencyDate)
        self.assertEqual(len(self.listCurrency),lenListCurrency)
        self.assertTrue("EUR" in self.listCurrency)
        self.assertTrue("ZAR" in self.listCurrency)
        self.assertTrue("CAD" in self.listCurrency)
        self.assertTrue("2018-04-12" in self.dictCurrencyDate)
        self.assertTrue("2018-04-16" in self.dictCurrencyDate)
        self.assertTrue("2018-07-10" in self.dictCurrencyDate)
        self.assertTrue("2018-05-29" in self.dictCurrencyDate)
        self.assertFalse("2018-06-02" in self.dictCurrencyDate)
        
    def testCalculateExchangeCurrency(self):
        aDate = "2018-07-10"
        srcCurrency = "ZAR"
        destCurrency = "CAD"
        amount = "77777777777777777777888.50"
        rateZar = Decimal('15.7331')
        rateCad = Decimal('1.5382') #leggendo dal file a mano (per test)
        euroAmount = Decimal(amount) / rateZar
        cadAmount =  euroAmount * rateCad
        self.assertEqual(Decimal("7604208819481079874782.98"),cadAmount.quantize(Decimal('0.01'), rounding=ROUND_HALF_EVEN))
        self.assertEqual(cadAmount.quantize(Decimal('0.01'), rounding=ROUND_HALF_EVEN),self.management.calculateExchangeCurrency(self.dictCurrencyDate[aDate],amount,srcCurrency,destCurrency))

        aDate = "2018-07-10"
        srcCurrency = "EUR"
        destCurrency = "CHF"
        amount = "33.33"
        rateChf = Decimal("1.1649")
        euroAmount = Decimal(amount)
        chfAmount =  euroAmount * rateChf
        self.assertEqual(chfAmount.quantize(Decimal('0.01'), rounding=ROUND_HALF_EVEN),self.management.calculateExchangeCurrency(self.dictCurrencyDate[aDate],amount,srcCurrency,destCurrency))
        self.assertEqual(Decimal("38.83"), chfAmount.quantize(Decimal('0.01'), rounding=ROUND_HALF_EVEN))


        aDate = "2018-04-12"
        srcCurrency = "JPY"
        destCurrency = "RUB"
        rateJPY = Decimal("136.54") #cambiato a mano
        rateRUB = Decimal("75.8954")
        amount = "999.99"
        euroAmount = Decimal(amount) / rateJPY
        rubAmount =  euroAmount * rateRUB
        self.assertEqual(rubAmount.quantize(Decimal('0.01'), rounding=ROUND_HALF_EVEN),self.management.calculateExchangeCurrency(self.dictCurrencyDate[aDate],amount,srcCurrency,destCurrency))
        self.assertEqual(Decimal("555.84"), rubAmount.quantize(Decimal('0.01'), rounding=ROUND_HALF_EVEN))

                         
if __name__ == "__main__":
    unittest.main()
