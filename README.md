# CurrencyConverter
* Exercise in python: an online currency converter.
* The exchange rates are in this link: https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml
* When the project starts it fetches the last xml file that contains the last 90 days exchange rates.

Install
-------
1. Install python 3.x
2. clone the repository: git clone https://Alessandro89@bitbucket.org/Alessandro89/currencyconverter.git
3. Install flask-restful framework: pip install flask-restful.  
   - (https://flask-restful.readthedocs.io/en/latest/installation.html)
4. download postman if you want

Run
---
1. Start the server  
   - Run on windows cmd in the project directory: python endpointConverter.py
2. With a browser or with postman, do a get call like this example:  
   - http://localhost:5000/convert?amount=999.99&src_currency=jpy&dest_currency=rub&reference_date=2018-07-10.  
   - Insert a date that is present in the xml file, which was mentioned by the link at the beginning  

Test
----
- Run on windows cmd in the project directory: python test_endpointConverter.py

