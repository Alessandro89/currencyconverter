from urllib.request import urlopen
import xml.etree.ElementTree as ET
import datetime
from flask import Flask
from flask_restful import Resource, Api, reqparse, abort
import logging
from decimal import *

app = Flask(__name__)
api = Api(app)

urlExchangeCurrency = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml"

class ManagementCurrency():
    def __init__(self,file):
        tree = ET.parse(file)
        root = tree.getroot()
        currencyDateList = None
        for child in root:
                if("Cube" in child.tag):
                        currencyDateList = child
        #usd,jpy,bgn..
        self.listCurrency = [currency.get("currency") for currency in currencyDateList[0]]
        self.listCurrency.append("EUR") #aggiungo currency euro
        #date:objectCurrency
        self.dictCurrencyDate = {currency.get("time"):currency for currency in currencyDateList}
        file.close()
    
    def calculateExchangeCurrency(self,exchange,amount,srcCurrency,destCurrency):
        #rate
        rateSrc = 1
        rateDest = 1
        #imposta il rate relativo alla currency (lasciera' 1 se e' euro)
        for currency in [currency for currency in exchange]:
            if currency.get("currency") == srcCurrency:
                rateSrc = currency.get("rate")
            if currency.get("currency") == destCurrency:
                rateDest = currency.get("rate")
        #Decimal numbers can be represented exactly
        amountEuro = Decimal(amount)/Decimal(str(rateSrc)) 
        amountDest = Decimal(amountEuro) * Decimal(str(rateDest))
        return amountDest.quantize(Decimal('0.01'), rounding=ROUND_HALF_EVEN)

class ConvertAPI(Resource):
    def __init__(self):
        self.managementCurrency = ManagementCurrency(urlopen(urlExchangeCurrency))
        self.listCurrency = self.managementCurrency.listCurrency
        self.dictCurrencyDate = self.managementCurrency.dictCurrencyDate
    def get(self):
        parser = reqparse.RequestParser()
        #float può avere delle approssimazione, dunque str
        parser.add_argument('amount', type = str, required = True, location = 'args') 
        parser.add_argument('src_currency',type = str, required = True, location = 'args')
        parser.add_argument('dest_currency',type = str, required = True, location = 'args')
        parser.add_argument('reference_date',type = str, required = True, location = 'args')
        args = parser.parse_args(strict = True)
        amount = args["amount"]
        srcCurrency = args["src_currency"].replace('"','').upper()
        destCurrency = args["dest_currency"].replace('"','').upper()
        referenceDate = args["reference_date"].replace('"','')
        if(Decimal(amount) < 0):
            abort(400, message = "the amount is negative")
        if(srcCurrency not in self.listCurrency):
            abort(400, message = "your src_currency is not present in the list of currency: ")
        if(destCurrency not in self.listCurrency):
            abort(400, message = "your dest_currency is not present in the list of currency")
        try:
            referenceDate = datetime.datetime.strptime(referenceDate, '%Y-%m-%d').strftime("%Y-%m-%d") #add zero-padded
        except ValueError:
            abort(400, message = "Incorrect data format, should be YYYY-MM-DD")
        #gli scambi di valute non sono presenti nei festivi..
        if referenceDate not in self.dictCurrencyDate:
            abort(400, message = "the date is not available, should be a workday and not older than 90 days")
            
        exchange =  self.dictCurrencyDate[referenceDate]
        amountDest = self.managementCurrency.calculateExchangeCurrency(exchange,amount,srcCurrency,destCurrency)
        #per evitare delle approssimazioni rappresento l'importo in stringa
        return {'amount': str(amountDest), 'currency': destCurrency}


api.add_resource(ConvertAPI, '/convert', endpoint = 'convert')
	
if __name__ == '__main__':
    app.run(debug=True)
